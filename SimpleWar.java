public class SimpleWar{
	public static void main(String[]args){
		Deck deck = new Deck();
		deck.shuffle();
		int player1Points = 0;
		int player2Points = 0;
		while(deck.length()>1){
			
		
			Card firstCard = deck.drawTopCard();
			Card secondCard = deck.drawTopCard();
			System.out.println("Player 1:" + player1Points +"\n Player 2:" + player2Points);
			System.out.println( firstCard+ ": " +firstCard.calculateScore() );
			System.out.println( secondCard + ": " + secondCard.calculateScore() );
			if (secondCard.calculateScore()> firstCard.calculateScore()){
				System.out.println("2 Won");
				player2Points++;
			}
			else{
				System.out.println("1 Won");
				player1Points++;
			}
			System.out.println("Player 1:" + player1Points +"\n Player 2:" + player2Points);
		}
		if (player1Points> player2Points){
			System.out.println("Congrats, player 1!");
		}
		if(player2Points> player1Points){
			System.out.println("Congrats, player 2!");
		}
		if(player2Points== player1Points){
			System.out.println("draw");
		}
	}
}