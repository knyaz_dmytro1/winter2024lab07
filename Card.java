public class Card{
	private String suit;
	private String value;
	public Card(String suit, String value){
		this.suit = suit;
		this.value = value;
	}
	public String getValue(){
		return this.value;
	}
	public String getSuit(){
		return this.suit;
	}
	public String toString(){
		return this.value + " of " + this.suit;
	}
	public double calculateScore(){
		String[] suits = new String[]{"spade", "club", "diamond", "heart"};
		String[] values = new String[]{"Ace","2","3","4","5","6","7","8","9","10","Jack","Queen", "King"};
		double score = 0;
		for( int i= 0 ; i< values.length; i++){
			if (values[i].equals(this.value)){
				score+=i+1;
				
			}
		}
		for( int i= 0 ; i< suits.length; i++){
			if (suits[i].equals(this.suit)){
				score+=(Double.valueOf(i)+1)/10;
			}
		}
		return score;
	}
}