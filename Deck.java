import java.util.Random;
public class Deck{
	private Card [] cards;
	private int numOfCards;
	Random rng;
	public Deck(){
		this.numOfCards = 52;
		this.rng = new Random();
		this.cards = new Card[this.numOfCards];
		String[] suits = new String[]{"heart", "spade", "club", "diamond"};
		String[] values = new String[]{"Ace","2","3","4","5","6","7","8","9","10","Jack","Queen", "King"};
		for (int i = 0 ; i < this.numOfCards; i++){
			this.cards[i] = new Card(suits[i/values.length], values[i%values.length]);
		}
	}
	public int length(){
		return this.numOfCards;
	}
	public Card drawTopCard(){
			numOfCards--;
			return cards[numOfCards];
	}
	public String toString (){
		String finalString = "";
		for(int i = 0 ; i< this.cards.length; i++){
			finalString += cards[i] + ", \n";
		}
		return finalString;
	}
	public void shuffle(){
		for (int i = 0; i< this.cards.length; i++){
			int number = this.rng.nextInt(this.cards.length);
			Card card=cards[i];
			cards[i] = cards[number];
			cards[number] = card;
		}
	}
	
}